import { Injectable } from '@angular/core';
import { __spreadArrays } from 'tslib';
import { ROWS, COLS, POINTS } from './constants';
import { IPiece } from './piece.model';

@Injectable({
  providedIn: 'root'
})
export class BoardService {

  constructor() { }

  valid(p: IPiece, board: number[][]): boolean {
    return p.shape.every((row, dy) => {
      return row.every((value, dx) => {
        let x = p.x + dx;
        let y = p.y + dy;
        return (
          this.isEmpty(value) ||
          (this.insideWalls(x) && 
          this.aboveFloor(y) &&
          this.notOccupied(board, x, y))
        );
      });
    });
  }

  rotate(piece: IPiece): IPiece {
    // Deep copy with JSON
    let p: IPiece = JSON.parse(JSON.stringify(piece));
    // Transpose matrix
    for (let y = 0; y < p.shape.length; ++y) {
      for (let x = 0; x < y; ++x) {
        [p.shape[x][y], p.shape[y][x]] = [p.shape[y][x], p.shape[x][y]];
      }
    }
    // Reverse the order of the columns.
    p.shape.forEach(row => row.reverse());
    return p;
  }

  getLineClearPoints(lines: number): number {
    return lines === 1 ? POINTS.SINGLE :
           lines === 2 ? POINTS.DOUBLE :
           lines === 3 ? POINTS.TRIPLE :
           lines === 4 ? POINTS.TETRIS : 0;
  }

  private aboveFloor(y: number): boolean {
    return y < ROWS;
  }
  
  private insideWalls(x: number): boolean {
    return x >= 0 && x < COLS;
  }

  private notOccupied(board: number[][], x: number, y: number): boolean {
    return board[y] && board[y][x] === 0;
  }

  private isEmpty(value: number): boolean {
    return value === 0;
  }
}
