import { Component, OnInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { BLOCK_SIZE, COLS, ROWS, KEY, COLORS, POINTS, LEVEL, LINES_PER_LEVEL } from '../constants';
import { BoardService } from '../board.service';
import { Piece, IPiece } from '../piece.model';

@Component({
  selector: 'game-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {

  @ViewChild('board', { static: true })
  canvas: ElementRef<HTMLCanvasElement>;
  ctx: CanvasRenderingContext2D;

  nextPiece: Piece;
  @ViewChild('next', { static: true })
  canvasNext: ElementRef<HTMLCanvasElement>;
  ctxNext: CanvasRenderingContext2D;

  animationRequestId: number;

  board: number[][];
  piece: Piece;

  points: number;
  lines: number;
  level: number;
  time: { start: number, elapsed: number, level: number };

  moves = {
    [KEY.LEFT]: (p: IPiece): IPiece => ({ ...p, x: p.x - 1 }),
    [KEY.RIGHT]: (p: IPiece): IPiece => ({ ...p, x: p.x + 1 }),
    [KEY.DOWN]: (p: IPiece): IPiece => ({ ...p, y: p.y + 1 }),
    [KEY.SPACE]: (p: IPiece): IPiece => ({ ...p, y: p.y + 1 }),
    [KEY.UP]: (p: IPiece): IPiece => this.service.rotate(p)
  }

  constructor(private service: BoardService) {}

  ngOnInit() {
    this.initBoard();
    this.initNext();
    this.resetGame();
  }

  @HostListener('window:keydown', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.keyCode === KEY.ENTER) {
      this.play();
    } else if (this.moves[event.keyCode]) {
      event.preventDefault();
      let nextPieceState = this.moves[event.keyCode](this.piece);
      if (event.keyCode === KEY.SPACE) {
        // Hard drop
        while (this.service.valid(nextPieceState, this.board)) {
          this.points += POINTS.HARD_DROP;
          this.piece.move(nextPieceState);
          nextPieceState = this.moves[KEY.DOWN](this.piece)
        }
      } else if (this.service.valid(nextPieceState, this.board)) {
        this.piece.move(nextPieceState);
        if (event.keyCode === KEY.DOWN) {
          this.points += POINTS.SOFT_DROP;
        }
      }
    }
  }

  initBoard() {
    this.ctx = this.canvas.nativeElement.getContext('2d');
    this.ctx.canvas.width = COLS * BLOCK_SIZE;
    this.ctx.canvas.height = ROWS * BLOCK_SIZE;
    // Scale so we don't need to give size on every draw.
    this.ctx.scale(BLOCK_SIZE, BLOCK_SIZE);
  }

  initNext() {
    this.ctxNext = this.canvasNext.nativeElement.getContext('2d');
    this.ctxNext.canvas.width = 4 * BLOCK_SIZE;
    this.ctxNext.canvas.height = 4 * BLOCK_SIZE;
    this.ctxNext.scale(BLOCK_SIZE, BLOCK_SIZE);
  }

  play() {
    this.resetGame();
    this.nextPiece = new Piece(this.ctx);
    this.piece = new Piece(this.ctx);
    this.nextPiece.drawNext(this.ctxNext);
    this.time.start = performance.now();

     // If we have an old game running a game then cancel the old
     if (this.animationRequestId) {
      cancelAnimationFrame(this.animationRequestId);
    }

    this.animate();
  }

  gameOver() {
    cancelAnimationFrame(this.animationRequestId);
    this.ctx.fillStyle = 'black';
    this.ctx.fillRect(1, 3, 8, 1.2);
    this.ctx.font = '1px Arial';
    this.ctx.fillStyle = 'red';
    this.ctx.fillText('GAME OVER', 1.8, 4);
  }

  resetGame() {
    this.board = this.getEmptyBoard();
    this.points = 0;
    this.lines = 0;
    this.level = 0;
    this.time = { start: 0, elapsed: 0, level: LEVEL[this.level] };
  }

  animate(now = 0) {
    this.time.elapsed = now - this.time.start;
    if (this.time.elapsed > this.time.level) {
      this.time.start = now; 
      if (!this.drop()) {
        this.gameOver();
        return;
      }
    }
    this.draw();
    this.animationRequestId = requestAnimationFrame(this.animate.bind(this));
  }

  drop() {
    let p = this.moves[KEY.DOWN](this.piece);
    if (this.service.valid(p, this.board)) {
      this.piece.move(p);
    } else {
      this.freeze();
      const clearedCount = this.clearLines();
      this.points += this.service.getLineClearPoints(clearedCount);
      this.lines += clearedCount;
      if (this.lines >= LINES_PER_LEVEL) {
        this.level++;
        this.lines -= LINES_PER_LEVEL;
        this.time.level = LEVEL[this.level];
      }
      if (this.piece.y === 0) {
        return false; // game over
      }
      this.piece = this.nextPiece;
      this.nextPiece = new Piece(this.ctx);
      this.nextPiece.drawNext(this.ctxNext);
    }
    return true;
  }

  draw() {
    this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
    this.piece.draw();
    this.drawBoard();
  }

  drawBoard() {
    this.board.forEach((row, y) => {
      row.forEach((value, x) => {
        if (value > 0) {
          this.ctx.fillStyle = COLORS[value - 1];
          this.ctx.fillRect(x, y, 1, 1);
        }
      })
    })
  }

  freeze() {
    this.piece.shape.forEach((row, y) => {
      row.forEach((value, x) => {
        if (value > 0) {
          this.board[this.piece.y + y][this.piece.x + x] = value;
        }
      })
    })
  }

  clearLines(): number {
    let lines = 0;
    this.board.forEach((row, y) => {
      if (row.every(value => value > 0)) {
        lines++;
        this.board.splice(y, 1); // remove the row
        this.board.unshift(Array(COLS).fill(0)) // Add a zero filled at the top.
      }
    });
    return lines;
  }

  getEmptyBoard(): number[][] {
    return Array.from({ length: ROWS }, () => Array(COLS).fill(0));
  }
}
